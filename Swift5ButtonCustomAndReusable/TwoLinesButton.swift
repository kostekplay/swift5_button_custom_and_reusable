////  TwoLinesButton.swift
//  Swift5ButtonCustomAndReusable
//
//  Created on 19/11/2020.
//  
//

import UIKit

struct TwoLineButtonViewModel {
    let firstLine: String
    let secondLine: String
}

final class TwoLinesButton: UIButton {
    
    private let firstLine: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .center
        label.textColor = .white
        label.font = .systemFont(ofSize: 20, weight: .semibold)
        return label
    }()
    
    private let secondLine: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .center
        label.textColor = .white
        label.font = .systemFont(ofSize: 18, weight: .regular)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(firstLine)
        addSubview(secondLine)
        clipsToBounds = true
        layer.cornerRadius = 8
        layer.borderWidth = 1
        layer.borderColor = UIColor.secondarySystemBackground.cgColor
        backgroundColor = .systemGreen
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with viewModel: TwoLineButtonViewModel) {
        firstLine.text = viewModel.firstLine
        secondLine.text = viewModel.secondLine
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        firstLine.frame = CGRect(x: 5, y: 0, width: frame.size.width - 10, height: frame.size.height/2)
        secondLine.frame = CGRect(x: 5, y: frame.size.height/2, width: frame.size.width - 10, height: frame.size.height/2)
    }
    
}
