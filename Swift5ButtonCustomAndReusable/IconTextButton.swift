////  IconTextButton.swift
//  Swift5ButtonCustomAndReusable
//
//  Created on 19/11/2020.
//  
//

import UIKit

struct IconTextViewModel {
    let text: String
    let image: UIImage?
    let backgroundColor: UIColor?
}

final class IconTextButton: UIButton {
    
    private let textLine: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .center
        label.textColor = .white
        label.font = .systemFont(ofSize: 20, weight: .semibold)
        return label
    }()
    
    private let iconImage: UIImageView = {
        let image = UIImageView()
        image.tintColor = .white
        image.contentMode = .scaleAspectFit
        image.clipsToBounds = true
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(textLine)
        addSubview(iconImage)
        clipsToBounds = true
        layer.cornerRadius = 8
        layer.borderWidth = 1
        layer.borderColor = UIColor.secondarySystemBackground.cgColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with viewModel: IconTextViewModel) {
        textLine.text = viewModel.text
        iconImage.image = viewModel.image
        backgroundColor = viewModel.backgroundColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        textLine.sizeToFit()
        let iconSize: CGFloat = 25
        let iconX: CGFloat = ((frame.size.width - textLine.frame.size.width - iconSize - 5) / 2)
        iconImage.frame = CGRect(x: iconX, y: (frame.size.height - iconSize)/2, width: iconSize, height: iconSize)
        textLine.frame = CGRect(x: iconX + iconSize + 5, y: 0, width: textLine.frame.size.width, height: frame.size.height)
    }
    
}
