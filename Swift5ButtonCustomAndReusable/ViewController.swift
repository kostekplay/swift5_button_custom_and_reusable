////  ViewController.swift
//  Swift5ButtonCustomAndReusable
//
//  Created on 19/11/2020.
//  
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let twoLineButton = TwoLinesButton(frame: CGRect(x: 0, y: 0, width: 250, height: 65))
        view.addSubview(twoLineButton)
        twoLineButton.center = view.center
        twoLineButton.configure(with: TwoLineButtonViewModel(firstLine: "Pierwsza Linia Buttona", secondLine: "Druga Linia Buttona Druga Linia"))
        
        let iconButton = IconTextButton(frame: CGRect(x: (view.frame.size.width - 250 ) / 2, y: 60, width: 250, height: 65))
        view.addSubview(iconButton)
        iconButton.configure(with: IconTextViewModel(text: "Go shop", image: UIImage(systemName: "cart"), backgroundColor: .systemOrange))
    }


}

